function checkEmail(email ){
    
    let format = new RegExp(/^[a-zA-Z0-9]+@[a-zA-Z.]+\.[a-zA-Z]{2,3}$/)
    // Mengecek apakah parameter email apakah TRUE(terdapat data) atau FALSE(data kosong) atau email sama dengan number
	if(!email || typeof email === 'number') {
        // mengembalikan nilai string 
      return 'error data/parameter kosong'
    } else {
        // jika parameter email sesuai format atau bernilai TRUE, maka mengembalikan return string email VALID
      if(format.test(email) === true) {
        return 'email valid'
        // jika parameter email tidak sesuai format atau bernilai FALSE, maka mengembalikan return string email INVALID
      } else if(format.test(email) === false){
          // deklarasi varible untuk menampung nilai indexOf pada At(@)
        let tanpaAt = email.indexOf("@")	
        // mengecek kondisi, jika At(@) kurang dari 1 maka return error: format tidak lengkap
        if(tanpaAt < 1) {
					return 'error: format tidak lengkap'
            // jika tidak, maka return email invalid
        } else {
          return 'email invalid'
        }
      }
    }
  
}

console.log(checkEmail('rifhiaputri@binar.co.id'))
console.log(checkEmail('rifhiaputri@binar.com'))
console.log(checkEmail('rifhiaputri@binar'))
console.log(checkEmail('rifhiaputri'))
console.log(checkEmail(322))
console.log(checkEmail())


