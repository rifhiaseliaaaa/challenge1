const dataPenjualanNovel = [
    {
      idProduk: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStock: 17,
    },
    {
      idProduk: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStock: 20,
    },
    {
      idProduk: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStock: 5,
    },
    {
      idProduk: 'BOOK002942',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStock: 56,
    },
  ]
    function getInfoPenjualan(dataPenjualan) {
    // deklarasi varible dengan object kosong
      let hasil = {}
      // deklarasi variable dengan total Modal, total Keuntungan, persentase keuntungan, produk terlaris, dan penulis terlaris
    let totalModal = 0,
        totalKeuntungan = 0,
        persentaseKeuntungan = 0,
        produkTerlaris, 
        penulisTerlaris;
      // deklarasi variable terlaris yang diisi oleh total terjual paling besar
      let terlaris = Math.max(...dataPenjualan.map((e) => e.totalTerjual))
      // pengulangan untuk menjumlahkan total modal dan total keuntungan
      for (let x = 0 ; x < dataPenjualan.length ; x++){
        totalModal = totalModal + ((dataPenjualan[x].totalTerjual + dataPenjualan[x].sisaStock) * dataPenjualan[x].hargaBeli)
          totalKeuntungan = totalKeuntungan + ((dataPenjualan[x].hargaJual - dataPenjualan[x].hargaBeli) * dataPenjualan[x].totalTerjual)
        // jika total terjual sama dengan terlaris
        if (dataPenjualan[x].totalTerjual == terlaris) {
            // maka produk terlaris diisi dengan property nama produk terlaris
          produkTerlaris = dataPenjualan[x].namaProduk ;
          // maka produk terlaris diisi dengan property nama penulis terlaris
          penulisTerlaris = dataPenjualan[x].penulis ;
        }
      }
      // menghitung persentase keuntungan dengan membagi total keuntungan dengan total modal lalu dikalikan 100
      persentaseKeuntungan = (totalKeuntungan / totalModal) * 100
      // memasukan property total keuntungan, total modal, persentase keuntungan, produk terlaris, dan penulis terlaris
      hasil = {
          // property total keuntungan diisi dengan totalkeuntungan 
        totalKeuntungan: 'Rp.' + totalKeuntungan,
        // properrty total modal diisi dengan totalmodal
        totalModal: 'Rp.' + totalModal,
        // property persentase keuntungan diisi dengan persentaseKeuntungan
        persentaseKeuntungan: persentaseKeuntungan + '%',
        // property produk terlaris diisi dengan produkTerlaris
        produkTerlaris: produkTerlaris,
        // property penulis terlaris diisi dengan penulisTerlaris
        penulisTerlaris: penulisTerlaris,
      }
      // mengembalikan object hasil
      return hasil
    }
  
  console.log(getInfoPenjualan(dataPenjualanNovel));