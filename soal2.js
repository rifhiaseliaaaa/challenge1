function checkTypeNumber(givenNumber){
    // Mengecek apakah parameter givenNumber apakah TRUE(terdapat data) atau FALSE(data kosong)
    if (!givenNumber) {
        // return adalah mengembalikan nilai, jika return dijalankan maka dia akan keluar dari function
      return 'Data kosong'
    } else {   
    // Mengecek Type data, Jika number maka jalankan perintah yang ada di dalam {}  
      if (typeof givenNumber === 'number') {
        // Pengecekan givenNumber JIKA sama dengan 0 maka return GENAP
        if (givenNumber % 2 === 0) {
            return 'GENAP';
        // Pengecekan givenNumber JIKA tidak sama dengan 0 maka return GANJIL
        } else if (givenNumber % 2 !== 0) {
            return 'GANJIL'
        }
        // JIKA type data bukan NUMBER maka return STRING 'INVALID Type Data'
    } else {
      return 'INVALID Type Data'
    }
    }
  }
  console.log(checkTypeNumber(10));
  console.log(checkTypeNumber(7));
  console.log(checkTypeNumber('7'));
  console.log(checkTypeNumber({}));
  console.log(checkTypeNumber([]));
  console.log(checkTypeNumber());