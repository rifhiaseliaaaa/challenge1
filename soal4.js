function isValidPassword (password) {
    let format = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$/)
    // Mengecek apakah parameter Password apakah TRUE(terdapat data) atau FALSE(data kosong) dan Password tidak sama dengan 0
    if(!password && password !== 0) {
        // maka mengembalikan nilai error data atau parameter kosong
         return 'error data/parameter kosong'
         // jika tidak
       } else {
        // jika parameter password sesuai format atau bernilai TRUE, maka mengembalikan return TRUE
         if(format.test(password) === true) {
           return true
           // jika parameter password tidak sesuai format atau bernilai FALSE, maka mengembalikan FALSE
         } else if(format.test(password) === false){
           return false
         }
       }
   }
   
   console.log(isValidPassword('Prau1409'))
   console.log(isValidPassword('prau1409'))
   console.log(isValidPassword('1409'))
   console.log(isValidPassword('409'))
   console.log(isValidPassword(0))
   console.log(isValidPassword())
   