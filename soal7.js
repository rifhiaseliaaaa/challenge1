const dataPenjualanPakAdi = [
    {
    namaProduct: 'Sepatu Futsal Nike Vapor Academy 9',
    hargaSatuan: 760000,
    kategori: 'Sepatu Sport',
    totalTerjual: 90
    },
    {
      namaProduct: 'Sepatu Warrior Tristan Black Brown High - Original',
      hargaSatuan: 960000,
      kategori: 'Sepatu Sneaker',
      totalTerjual: 37
    },
    {
      namaProduct: 'Sepatu Warrior Tristan Maroon High - Original',
      hargaSatuan: 360000,
      kategori: 'Sepatu Sneaker',
      totalTerjual: 90
    },
    {
      namaProduct: 'Sepatu Warrior Rainbow Tosca Cordury - [BNIB] Original',
      hargaSatuan: 120000,
      kategori: 'Sepatu Sport',
      totalTerjual: 90
    },
  ]
  
  function hitungTotalPenjualan(dataPenjualan) {
    let hasil = 0
  //   Pengulangan terjadi apabila kondisi dalam pengulangan bernilai TRUE
  //   let x = 0; inisialiasi
  // x < dataPenjualan.length = KONDISI 
  //   x++ = x + 1
    for (let X = 0 ; X < dataPenjualan.length ; X++) {
    // hasil ditambah sama dengan semua totalTerjual yang terdapat pada array dataPenjualan
    // hasil + totalTerjual
    // 0 + 90
    // 90 + 37
    // 127 + 90
    // 307

      hasil += dataPenjualan[X].totalTerjual
    }
    // mengembalika hasil penjumlahan semua property totalTerjual
    return hasil
  }
  
  console.log(hitungTotalPenjualan(dataPenjualanPakAdi))