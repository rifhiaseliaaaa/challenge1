function getAngkaTerbesarKedua(dataNumbers) {

    if (!Array.isArray(dataNumbers)) { // => jika parameter yang diterima bukan Array
        return "Error: Please insert the number correctly! this input only receive array data"; // maka mengembalikan nilai dengan string
    }   else if (dataNumbers.length < 1) { // cek apakah array [dataNumbers] berisikan kurang dari 1
        return "Error: Please Insert 2 or more number!"
    }   else
        // console.log(dataNumbers.Length)
      dataNumbers.sort((a, b) => {return a - b}).reverse()

        console.log(dataNumbers)
        // Mengambil angka terbesar
        let angkaTerbesar = Math.max(...dataNumbers); // 9

        // Mengambil total angka terbesar
        let totalAngkaTerbesar = dataNumbers.filter((dataNumbers) => dataNumbers == angkaTerbesar).length //3

        // untuk mengetahui letak angka terbesar kedua di [dataNumbers] => angkaTerbesarKedua + totalAngkaTerbesar


        return dataNumbers[totalAngkaTerbesar];
}

    // dataNumbers.sort().reverse()
    // console.log(dataNumbers)
    // return dataNumbers[1]

const dataAngka = [9,9,4,7,7,5,3,2,2,6]

// const test = [1,2]

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());