
// Deklasrasi function dengan nama changeWord yang dimana function tersebut memiliki parameter selectedText, changeText, text
// parameter menrima data dari pemanggilan function nya itu sendiri
function changeWord (selectedText, changeText, text){
    // Setiap function harus mengembalikan nilai (return)
    return text.replace(selectedText, changeText)
}
// kalimat nama variabel
// 'Wafiq menyukai birama' value/nilai dari variabel
const kalimat1 = 'Wafiq menyukai birama'
const kalimat2 = 'Gunung sindoro merupakan salah satu gunung di Indonesia'

// console.log() buat nampilin output di console/terminal
// changeWord() memanggil function
// ('menyukai','membenci',kalimat1) adalah mengisi prameter
console.log(changeWord('menyukai','membenci',kalimat1));
console.log(changeWord('sindoro','bromo',kalimat2));