function getSplitName(personName) {
    // Membuat variabel dengan tipe data objek yang diisi objek kosong
    let hasil={}
    
    // Pengecekan Tipe data personName JIKA STRING maka jalankan perintah yang ada pada {}
    if(typeof personName === 'string')
      {
        // variabel nameSplit digunakan untuk menampung data personName yang telah di ubah menjadi array
        // .split(' ') meminta ke javascript untuk memisahkan semua nilai string yang mengandung spasi
        let nameSplit = personName.split(' ')
        // Pengecekan panjang nama 
        if (nameSplit.length >3){
          return 'nama terlalu panjang'
        } 
        // JIKA panjang nama sama dengan 3 maka
        else if (nameSplit.length === 3){
        // hasil diisikan properti firstName dengan indeks 0 array nameSplit, middle diisi dengan indeks ke 1, lastName diisi indeks ke 2
          hasil = {
            firstName : nameSplit[0],
            MiddleName : nameSplit[1],
            LastName : nameSplit[2]
         }
        // JIKA panjang nama sama dengan 2 maka
            } else if (nameSplit.length ===2){
        // hasil diisikan properti firstName dengan indeks 0 array nameSplit, middle diisi dengan indeks null, lastName diisi indeks ke 1
            hasil = {
              firstName : nameSplit[0],
                MiddleName : null,
                LastName : nameSplit[1]
            } 
        // JIKA panjang nama sama dengan 1 maka
          } else if (nameSplit.length === 1){
        // hasil diisikan properti firstName dengan indeks 0 array nameSplit, middle diisi dengan indeks null, lastName diisi indeks null
            hasil = {
              firstName : nameSplit[0],
                MiddleName : null,
                LastName : null,
            }
        }
        // jika type data invalid 
      } else {
        return 'Invalid Type Data'
      }
    return hasil 
  }
  
  console.log(getSplitName("Wafiq Laura Clarizza"))
  console.log(getSplitName("Nabilah azzura"))
  console.log(getSplitName("Nugroho"))
  console.log(getSplitName("Nugroho Kuncoro"))
  console.log(getSplitName(9))
  